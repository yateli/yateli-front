angular.module('yateliApp').factory('jeuConcoursService', ['$http', '$httpParamSerializer', '$timeout', 'ApiService', 'userService', function($http, $httpParamSerializer, $timeout, ApiService, userService) {
    "use strict";

    return{
        getAllJeuxConcours() {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/jeuConcours/recupererTousLesConcours",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        participateJeuConcours(id) {
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/jeuConcours/participerJeuConcours?idJeuConcours="+id,
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        }
    };

}]);