angular.module('yateliApp').factory('userService', ['$http', '$httpParamSerializer', '$cookies', '$timeout', 'ApiService', "$q", function($http, $httpParamSerializer, $cookies, $timeout, ApiService, $q) {
    "use strict";

    let self = this;

    self.user = undefined;

    self.token = "";

    return{

        disconnectUser: function(redirect){
            $cookies.remove("yateli_access_token");
            if(redirect){
                window.location.href = "/#!/map?ts=" + new Date().getTime();
                window.location.reload(true);
            }
        },

        getUserObject: function(){
            return self.user;
        },

        getToken: function(){
            return self.token;
        },

        getUser: function () {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/user/getUser",
                headers: ApiService.getHeadersUrlencoded(self.token)
            };

            return $http(req).then(function (data) {
                self.user = data.data;
                return data.data;
            }, function (error) {
                return error;
            });
        },

        setToken: function(token) {
            self.token = token;
        },

        setUser: function (user) {
            self.user = user;
        },

        setCart: function(cart) {
            self.user.panier = cart;
        },

        connectUser: function (email, password) {

            let infos = {
                grant_type:"password",
                username: email,
                password: password
            };

            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/oauth/token",
                headers: ApiService.getHeadersForToken(),
                data: $httpParamSerializer(infos)
            };

            return $http(req).then(function (data) {
                self.token = data.data.access_token;
                $http.defaults.headers.common.Authorization = 'Bearer ' + data.data.access_token;
                $cookies.put("login", infos.username);
                $cookies.put("yateli_access_token", JSON.stringify(data.data));
            });

        },

        passwordForgot: function (login) {
            let infos = { login: login };
            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/api/user/motDePasseOublie",
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=utf-8"
                },
                data: $httpParamSerializer(infos)
            };
            return $http(req).then(function (data) {
                return data.data.message === "OK";
            });
        },

        verifCodePasswordForgot: function (login, code) {
            let infos = {
                login: login,
                code: code
            };
            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/api/user/verificationCode",
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=utf-8"
                },
                data: $httpParamSerializer(infos)
            };
            return $http(req).then(function (data) {
                return data.data.message === "OK";
            });
        },

        editPasswordWithCode: function (code, newPassword){
            let infos = {
                code: code,
                nouveauPassword: newPassword
            };
            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/api/user/nouveauPasswordAvecCode",
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=utf-8"
                },
                data: $httpParamSerializer(infos)
            };
            return $http(req).then(function (data) {
                return data.data.message === "OK";
            });
        },

        getCart: function () {
            if (self.user && self.user.panier) {
                return self.user.panier;
            } else {
                return null;
            }
        },

        addToCart: function (item) {
            if (self.user.cart) {

                let index = self.user.cart.findIndex(x => {
                    return x.id === item.id;
                });
                if(index !== -1) {
                    self.user.cart[index].quantite++;
                } else {
                    item.quantite = 1;
                    self.user.cart.push(item);
                }
                return true;
            } else {
                return false;
            }
        },

        deleteFromCart: function(item) {
            if (self.user.cart) {
                let index = self.user.cart.produits.findIndex(prod => {
                    return item.id === prod.id;
                });
                if (index > -1) {
                    self.user.cart.produits.splice(index, 1);
                }
            }
        },

        removeFromCart: function (item) {
            if (self.user.cart) {
                let index = self.user.cart.findIndex(x => {
                    return x.id === item.id;
                });
                if(index !== -1) {
                    self.user.cart.splice(index, 1);
                } else {
                    return false;
                }
                return true;
            } else {
                return false;
            }
        },

        updateQty: function (product_id, qty) {
            if (self.user.panier) {
                let product = self.user.panier.produits.find(x => {
                    return x.id === product_id;
                });
                if (product) {
                    product.quantite = qty;
                }
            }
        },

        getUserByType: function (userType) {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/user/getUserSelonRole/" + userType,
                headers: ApiService.getHeadersUrlencoded(this.getToken()),
                access_token: self.token
            };

            return $http(req).then(function (data) {
                console.log("data in get user", data);
            });
        },

        subscribeUser: function () {
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/user/abonnerUser",
                headers: ApiService.getHeadersUrlencoded(this.getToken()),
                access_token: self.token
            };

            return $http(req).then(function (data) {
                self.user = data.data;
                return data.data;
            });
        },

        activateAccount: function (login, password) {
            let infos = {
                login: login,
                password: password
            };
            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/api/user/activerCompte",
                headers: ApiService.getHeadersUrlencoded(this.getToken()),
                data: $httpParamSerializer(infos),
                access_token: self.token
            };

            return $http(req).then(function (data) {
                return data;
            }).catch(function (error) {
                return error;
            });
        },

        createAccount: function (user) {
            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/api/user/creer",
                headers: ApiService.getHeadersUrlencoded(this.getToken()),
                data: user
            };
            delete req.headers['Authorization'];
            req.headers['Content-type'] = 'application/json';
            return $http(req,user).then(function (data) {
                return data;
            });
        },

        unsubscribeUser: function () {
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/user/desabonnerUser",
                headers: ApiService.getHeadersUrlencoded(this.getToken()),
                access_token: self.token
            };

            return $http(req).then(function (data) {
                console.log("data in get user", data);
            });
        },

        getAllUsers: function () {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/user/getAllUsers",
                headers: ApiService.getHeadersUrlencoded(this.getToken()),
                access_token: self.token
            };

            return $http(req).then(function (data) {
                console.log("data in get user", data);
            });
        },

        getTokenWebSocket: function () {
            if(self.token){
                let req = {
                    method: 'GET',
                    url: ApiService.getBaseApiUrl() + "/api/user/getTokenWebsocket",
                    headers: ApiService.getHeadersUrlencoded(this.getToken()),
                    access_token: self.token
                };

                return $http(req).then(function (data) {
                    return data.data.message;
                });
            }else{
                let deferred = $q.defer();
                deferred.resolve(false);
                return deferred.promise;
            }
        },

        getConnectedUsersByRole: function (role) {
            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/api/user/getUsersConnectesSelonRole/" + role,
                headers: ApiService.getHeadersUrlencoded(this.getToken()),
                access_token: self.token
            };

            return $http(req).then(function (data) {
                console.log("data in get user", data);
            });
        },

        logoutUser: function (login, reason) {
            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/api/user/logoutSpecificUser",
                headers: ApiService.getHeadersUrlencoded(this.getToken())
            };

            return $http(req).then(function (data) {
                console.log("data in logout user", data);
                return data;
            });
        },

        updateUser: function (data) {
            let req = {
                method: 'PUT',
                data: data,
                url: ApiService.getBaseApiUrl() + "/api/user/modifierUser",
                headers: ApiService.getHeadersJson(this.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            });
        },

        deleteUser: function () {
            let req = {
                method: 'DELETE',
                url: ApiService.getBaseApiUrl() + "/api/user/supprimerUser",
                headers: ApiService.getHeadersUrlencoded(this.getToken())
            };

            return $http(req).then(function (data) {
                console.log("data in get user", data);
                return data.data;
            });
        },

        deleteSpecificUser: function (login) {
            let req = {
                method: 'DELETE',
                url: ApiService.getBaseApiUrl() + "/api/user/supprimerSpecificUser?login=" + login,
                headers: ApiService.getHeadersUrlencoded(this.getToken())
            };

            return $http(req).then(function (data) {
                console.log("data in get user", data);
                return data.data;
            });
        }

    };

}]);