angular.module('yateliApp').factory('PayementService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    return {
        payOrder(orderId) {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/paiement/payerCommande/" + orderId,
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                console.log("data in add to cart", data);
                return data;
            });
        },

        returnPaiement(order) {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/paiement/returnPaiement?" + $httpParamSerializer(order),
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            });
        }
    };

}]);