angular.module('yateliApp').factory('statistiquesService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    return {
        generateExcel(date, login, fileName){

            let body = {
                nameFile: fileName,
                minimumDate: date,
                login: login
            };

            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/statistiques/genererExcelReponses?" + $httpParamSerializer(body),
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req, {responseType: 'arraybuffer'}).then(function (data) {
                let binaryString = window.atob(data.data);
                let binaryLen = binaryString.length;
                let bytes = new Uint8Array(binaryLen);
                for (let i = 0; i < binaryLen; i++) {
                    bytes[i] = binaryString.charCodeAt(i);
                }
                return bytes;
            });
        }

    };

}]);