angular.module('yateliApp').factory('NotificationService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    let self = this;

    let service = {};

    self.notifications = [];

    service.getNotifications = function (){
        return self.notifications;
    };

    service.initialize = function () {
        let req = {
            method: 'GET',
            url: ApiService.getBaseApiUrl() + "/api/notification/recupererNotifications",
            headers: ApiService.getHeadersJson(userService.getToken())
        };

        return $http(req).then(function (data) {
            self.notifications = data.data;
            return data.data;
        });
    };

    service.removeNotification = function (id) {
        let req = {
            method: 'DELETE',
            url: ApiService.getBaseApiUrl() + "/api/notification/supprimerNotification/" + id,
            headers: ApiService.getHeadersJson(userService.getToken())
        };

        return $http(req).then(function (data) {
            let index = self.notifications.findIndex(n => n.id === id);
            self.notifications.splice(index, 1);
            return data.data;
        });
    };

    service.seeNotification = function (id) {
        let req = {
            method: 'PUT',
            url: ApiService.getBaseApiUrl() + "/api/notification/notificationVue/" + id,
            headers: ApiService.getHeadersJson(userService.getToken())
        };

        return $http(req).then(function (data) {
            let notif = self.notifications.find(n => n.id === id);
            notif.vu = true;
            return data.data;
        });
    };

    service.addNotification = function (notification) {
        self.notifications.push(notification);
    };

    return service;

}]);