angular.module('yateliApp').factory('ToasterService', ['$http', '$q', '$timeout', "ApiService", "userService", "toaster", function($http, $q, $timeout, ApiService, userService, toaster){
    "use strict";

    return {
        success(title, body, timeout, showCloseButton) {
            toaster.pop({
                type: 'success',
                title: title,
                body: body,
                timeout: timeout,
                showCloseButton: showCloseButton
            });
        },

        error(title, body, timeout, showCloseButton) {
            toaster.pop({
                type: 'error',
                title: title,
                body: body,
                timeout: timeout,
                showCloseButton: showCloseButton
            });
        },

        warning(title, body, timeout, showCloseButton) {
            toaster.pop({
                type: 'warning',
                title: title,
                body: body,
                timeout: timeout,
                showCloseButton: showCloseButton
            });
        },

        info(title, body, timeout, showCloseButton) {
            toaster.pop({
                type: 'info',
                title: title,
                body: body,
                timeout: timeout,
                showCloseButton: showCloseButton
            });
        },

        wait(title, body, timeout, showCloseButton) {
            return toaster.pop({
                type: 'wait',
                title: title,
                body: body,
                timeout: timeout,
                showCloseButton: showCloseButton
            });
        },

        html(type, title, body, timeout, showCloseButton) {
            toaster.pop({
                type: type,
                title: title,
                body: body,
                bodyOutputType: 'trustedHtml',
                timeout: timeout,
                showCloseButton: showCloseButton
            });
        },

        clear(toasterInstance){
            toaster.clear(toasterInstance);
        }

    };

}]);