angular.module('yateliApp').factory('cartService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    return {
        addToCart(item){

            let body = {
                quantite: item.quantite,
                reference: item.reference
            };


            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/panier/ajoutProduit?" + $httpParamSerializer(body),
                headers: ApiService.getHeadersUrlencoded(userService.getToken())/*,
                data: $httpParamSerializer(body)*/
            };

            return $http(req, body).then(function (data) {
                return data;
            });
        },

        deleteFromCart(item){
            let body = {
                quantite: item.quantite,
                reference: item.produit.reference
            };
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/panier/supprimerProduit?" + $httpParamSerializer(body),
                headers: ApiService.getHeadersUrlencoded(userService.getToken())/*,
                data: $httpParamSerializer(body)*/
            };
            return $http(req).then(function (data) {
                return data;
            }, function (error) {
                return error;
            });
        },

        updateQty: function (product) {
            let body = {
                quantite: product.quantite,
                reference: product.produit.reference
            };
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/panier/modifierProduit?" + $httpParamSerializer(body),
                headers: ApiService.getHeadersUrlencoded(userService.getToken())/*,
                data: $httpParamSerializer(body)*/
            };
            return $http(req, body).then(function (data) {
                return data;
            }, function (error) {
                console.error("error updating qty", error);
            });
        }
    };

}]);