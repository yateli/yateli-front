angular.module('yateliApp').factory('OrderService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    return {
        order(order) {
            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/api/commande/passerCommande?access_token=" + userService.getToken(),
                headers: {
                    "Content-type": "application/json; charset=utf-8"
                },
                data: order
            };

            return $http(req, order).then(function (data) {
                console.log("data in order", data);
                return data;
            }, function (error) {
                console.error("error in order", error);
                return error;
            });
        }
    };

}]);