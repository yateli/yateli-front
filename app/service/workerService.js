angular.module('yateliApp').factory('WorkerService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    return {
        acceptWorker(workerLogin) {
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/artisan/acceptation?loginArtisan=" + workerLogin,
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        authorizeWorker(w_id) {
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/artisan/autoriserArtisan?idArtisan=" + w_id,
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        declineWorker(mail, reason) {
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/artisan/refuser?" + $httpParamSerializer({'loginArtisan': mail, 'raison': reason}),
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        blockWorker(w_id, reason) {
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/artisan/bloquerArtisan?" + $httpParamSerializer({'idArtisan': w_id, 'raison': reason}),
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        workerDemand(worker) {
            return $http.post(ApiService.getBaseApiUrl() + "/api/artisan/demande", worker, ApiService.getPayloadCTUndefined(userService.getToken()))
                .then(function (response) {
                    return response.data;
                });
        },

        workersToAccept() {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/artisan/getArtisansAaccepter",
                headers: ApiService.getHeadersUrlencoded(userService.token)
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        getAllWorkers() {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/artisan/getTousLesArtisans",
                headers: ApiService.getHeadersUrlencoded(userService.token)
                // access_token: userService.token
            };

            return $http(req).then(function (data) {
                return data;
            });
        },

        updateWorker(data) {

            return $http.put(ApiService.getBaseApiUrl() + "/api/artisan/modifier", data, ApiService.getPayloadCTUndefined(userService.getToken()))
                .then(function (response) {
                    return response;
                });
        },

        getImage: function (idWorker) {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/artisan/recupererImage/" + idWorker,
                headers: ApiService.getHeadersMultipartFormdata(userService.getToken())
            };

            return $http(req, {responseType: 'arraybuffer'}).then(function (data) {
                return data;
            });
        },

        getAllWorkersToAccept() {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/artisan/getArtisansAaccepter",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
                // access_token: userService.token
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },
    };

}]);