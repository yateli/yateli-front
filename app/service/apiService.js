angular.module('yateliApp').factory('ApiService', ['$http', '$q', '$timeout', function($http, $q, $timeout){
    "use strict";

    return {
        getEncoding: function () {
            return btoa("yateli-front:yAt@lY");
        },

        getOriginUrl: function () {
            return "https://www.yateli.fr";
            //return "http://localhost:8080";
        },

        getBaseApiUrl: function () {
            return "https://www.yateli.fr/yateli";
            //return "http://localhost:8080";
        },

        getHeadersUrlencoded: function (token) {
            return {
                "Content-type": "application/x-www-form-urlencoded; charset=utf-8",
                "Authorization": "Bearer " + token
            };
        },

        getHeadersJson: function (token) {
            return {
                "Content-type": "application/json; charset=utf-8",
                "Authorization": "Bearer " + token
            };
        },

        getHeadersMultipartFormdata: function (token) {
            return {
                "Content-type": "multipart/form-data; charset=utf-8; boundary=Yateli",
                "Authorization": "Bearer " + token
            };
        },

        getPayloadCTUndefined: function (token) {
            return {
                transformRequest: angular.identity,
                headers: {
                    "Content-Type": undefined,
                    "Authorization": "Bearer " + token
                }
            };
        },

        getPayloadMultipart: function (token) {
            return {
                transformRequest: angular.identity,
                headers: {
                    "Content-Type": 'multipart/form-data; boundary=Yateli;',
                    "Authorization": "Bearer " + token
                }
            };
        },

        getHeadersForToken: function () {
            return {
                "Authorization": "Basic " + this.getEncoding(),
                "Content-type": "application/x-www-form-urlencoded; charset=utf-8"
            };
        }
    };

}]);