angular.module('yateliApp').factory('interrogationService', ['$http', 'ApiService', 'userService', function($http, ApiService, userService){
    "use strict";

    return {
        getQuestions(){

            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/interrogation/recupererQuestions"
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        getToutesQuestion(){

            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/interrogation/recupererToutesQuestions",
                headers: ApiService.getHeadersJson(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            })

        },

        createQuest(quest){

            let req = {
                method: 'POST',
                data: quest,
                url: ApiService.getBaseApiUrl() + "/api/interrogation/creerQuestion",
                headers: ApiService.getHeadersJson(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        activateQuest(id){

            let req = {
                method: 'POST',
                data: {
                    "idQuestions": [
                        id
                    ]
                },
                url: ApiService.getBaseApiUrl() + "/api/interrogation/activerQuestions",
                headers: ApiService.getHeadersJson(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        deActivateQuest(id){

            let req = {
                method: 'POST',
                data: {
                    "idQuestions": [
                        id
                    ]
                },
                url: ApiService.getBaseApiUrl() + "/api/interrogation/desactiverQuestions",
                headers: ApiService.getHeadersJson(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        }

    };

}]);