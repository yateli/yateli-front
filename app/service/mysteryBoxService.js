angular.module('yateliApp').factory('MysteryboxService', ['$http', '$httpParamSerializer', '$q', '$timeout', "ApiService", "userService", function($http, $httpParamSerializer, $q, $timeout, ApiService, userService){
    "use strict";

    return {
        getMysteryBox() {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/colismystere/getColisMystere",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            });
        }
    };

}]);