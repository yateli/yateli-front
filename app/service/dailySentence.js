angular.module('yateliApp').factory('SentenceService', ['$http', '$q', '$timeout', "ApiService", "userService", '$httpParamSerializer', function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    let self = this;

    self.dailySentenceImage = {};
    self.dailySentenceImage.sentence = "";
    self.dailySentenceImage.urlBackground = "";

    return {

        getSentenceService(){
            return self.dailySentenceImage;
        },

        setSentence(sentence){
            self.dailySentenceImage.sentence = sentence;
        },

        setBackground(urlImage){
            self.dailySentenceImage.urlBackground = urlImage;
        },

        /**
         * add daily sentence
         * @param item has to be {phrase: <value>}
         * @returns {Q.Promise<any> | webdriver.promise.Promise<any> | Q.IPromise<any> | * | PromiseLike<T | never> | Promise<T | never>}
         */
        addSentence(item){
            let req = {
                method: 'POST',
                url: ApiService.getBaseApiUrl() + "/api/phraseDuJour/creer?" + $httpParamSerializer(item),
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            });
        },

        getSentence(){
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/phraseDuJour/getPhraseDuJour",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                if(data.data.phrase && data.data.urlBackground){
                    self.dailySentenceImage.sentence = data.data.phrase;
                    self.dailySentenceImage.urlBackground = data.data.urlBackground;
                }else{
                    self.dailySentenceImage.sentence = "Bienvenue sur Yateli";
                    self.dailySentenceImage.urlBackground = "https://images.pexels.com/photos/618491/pexels-photo-618491.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260";
                }
                return self.dailySentenceImage;
            }).catch(function () {
                self.dailySentenceImage.sentence = "Bienvenue sur Yateli";
                self.dailySentenceImage.urlBackground = "https://images.pexels.com/photos/618491/pexels-photo-618491.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260";
                return self.dailySentenceImage;
            });
        },

        getAllSentences(){
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/phraseDuJour/getToutesPhrasesDuJour",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            });
        },

        updateSentence(sentence){
            let req = {
                method: 'PUT',
                url: ApiService.getBaseApiUrl() + "/api/phraseDuJour/modifier",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                console.log("data in add to cart", data);
                return data;
            });
        },

        deleteSentence(sentence){
            let req = {
                method: 'DELETE',
                url: ApiService.getBaseApiUrl() + "/api/phraseDuJour/supprimer",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            });
        }
    };

}]);