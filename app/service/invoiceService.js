angular.module('yateliApp').factory('FactureService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    return {
        getInvoice(factureId) {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/facture/getFacture/" + factureId,
                headers: ApiService.getHeadersMultipartFormdata(userService.getToken())
            };

            return $http(req, {responseType: 'arraybuffer'}).then(function (data) {
                let binaryString = window.atob(data.data);
                let binaryLen = binaryString.length;
                let bytes = new Uint8Array(binaryLen);
                for (let i = 0; i < binaryLen; i++) {
                    bytes[i] = binaryString.charCodeAt(i);
                }
                return bytes;
            });
        },

        getAllInvoices() {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/facture/getListFactureUser",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            });
        }
    };

}]);