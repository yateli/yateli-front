angular.module('yateliApp').factory('CountryService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    return {
        getCountries() {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/pays/recupererPays",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            });
        }
    };

}]);