angular.module('yateliApp').factory('webSocketService', ['$http', '$q', '$timeout', 'userService', 'ApiService', 'NotificationService', 'SweetAlert', 'SentenceService', 'ToasterService', function($http, $q, $timeout, userService, ApiService, NotificationService, SweetAlert, SentenceService, ToasterService){
    "use strict";

    let service = {}, listener = $q.defer(), socket = {
        client: null,
        stomp: null
    }, messageIds = [];

    service.RECONNECT_TIMEOUT = 30000;
    service.SOCKET_URL = ApiService.getOriginUrl() + "/socket-yateli";
    service.USER_URL = "/user/event";
    service.estConnecte = false;

    const typeDeconnexion = "DECONNEXION";
    const typeCompteDesactive = "COMPTE_DESACTIVE";
    const typePhraseDuJourModifiee = "EDITION_PHRASE_JOUR";
    const typeDemandeArtisan = "DEMANDE_ARTISAN";
    const typeAcceptationArtisan = "ACCEPTATION_ARTISAN";
    const typeBlocageArtisan = "BLOCAGE_ARTISAN";

    service.receive = function() {
        return listener.promise;
    };

   /* service.send = function(message) {
        let id = Math.floor(Math.random() * 1000000);
        socket.stomp.send(service.CHAT_BROKER, {
            priority: 9
        }, JSON.stringify({
            message: message,
            id: id
        }));
        messageIds.push(id);
    };
*/
    let reconnect = function() {
        $timeout(function() {
            initialize();
        }, this.RECONNECT_TIMEOUT);
    };

    let processMessage = function (data) {
        let alerte = JSON.parse(data.body).alerte;
        let eventType = alerte.type;
        let message = alerte.message;

        switch(eventType){
            case typeDeconnexion:
                service.disconnect();
                userService.disconnectUser(false);
                NotificationService.addNotification(alerte);
                let dialogDeconnexion = {
                    title : "Déconnexion forcée",
                    type : 'warning',
                    showCancelButton : false
                };
                SweetAlert.alert(message, dialogDeconnexion)
                    .then(function () {
                        window.location.href = "/#!/map?ts=" + new Date().getTime();
                        window.location.reload(true);
                    }, function () {
                        window.location.href = "/#!/map?ts=" + new Date().getTime();
                        window.location.reload(true);
                    });
                break;

            case typeCompteDesactive:
                service.disconnect();
                userService.disconnectUser(false);
                NotificationService.addNotification(alerte);
                let dialogCompteDesactive = {
                    title : "Compte Désactivé",
                    type : 'warning',
                    showCancelButton : false
                };
                SweetAlert.alert(message, dialogCompteDesactive)
                    .then(function () {
                        window.location.href = "/#!/map?ts=" + new Date().getTime();
                        window.location.reload(true);
                    }, function () {
                        window.location.href = "/#!/map?ts=" + new Date().getTime();
                        window.location.reload(true);
                    });
                break;

            case typeBlocageArtisan:
                service.disconnect();
                userService.disconnectUser(false);
                NotificationService.addNotification(alerte);
                let dialogCompteDesactiveArtisan = {
                    title : "Compte Désactivé",
                    type : 'warning',
                    showCancelButton : false
                };
                SweetAlert.alert(message, dialogCompteDesactiveArtisan)
                    .then(function () {
                        window.location.href = "/#!/map?ts=" + new Date().getTime();
                        window.location.reload(true);
                    }, function () {
                        window.location.href = "/#!/map?ts=" + new Date().getTime();
                        window.location.reload(true);
                    });
                break;

            case typePhraseDuJourModifiee:
                SentenceService.setSentence(message.phrase);
                SentenceService.setBackground(message.urlBackground);
                break;

            case typeDemandeArtisan:
                NotificationService.addNotification(alerte);
                let messageUtilisateur = 'L\'utilisateur ' + message.user.mail + ' souhaite devenir artisan';
                ToasterService.warning('Demande Artisan', messageUtilisateur, 6000, true);
                break;

            case typeAcceptationArtisan:
                NotificationService.addNotification(alerte);
                ToasterService.warning('Acceptation Artisan', 'Un administrateur vous a accepté, une reconnexion est nécessaire.\nVous serez déconnecté dans 5 secondes', 4500, true);
                setTimeout(function () {
                    userService.disconnectUser(true);
                }, 5000);
                break;

            default:
                console.error("Evenement WebSocket reçu sans traitement :\nEventType : " + eventType + "\nMessage : " + message);
        }
    };

    let startListener = function() {
        socket.stomp.subscribe(service.USER_URL, function (data) {
            processMessage(data);
        });
    };

    service.initialize = function() {
        userService.getTokenWebSocket().then(function (tokenWS) {
            let user = userService.getUserObject();
            if(tokenWS && user){
                let header = {
                    login : user.mail,
                    token : tokenWS
                };
                socket.client = new SockJS(service.SOCKET_URL);
                socket.stomp = Stomp.over(socket.client);
                socket.stomp.debug = null; //block log
                socket.stomp.connect(header, startListener);
                service.estConnecte = true;
                socket.stomp.onclose = reconnect;
            }
        });
    };

    service.disconnect = function () {
        if(service.estConnecte){
            socket.stomp.disconnect();
            service.estConnecte = false;
        }
    };

    return service;

}]);