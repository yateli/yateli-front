angular.module('yateliApp').factory('tagService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    return {
        getAllTags(){

            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/tag/recupererTousTags",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            }, function (error) {
                return error;
            });
        }

    };

}]);