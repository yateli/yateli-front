angular.module('yateliApp').factory('productService', ['$http', '$q', '$timeout', "ApiService", "userService", "$httpParamSerializer", function($http, $q, $timeout, ApiService, userService, $httpParamSerializer){
    "use strict";

    return {
        getAllProducts: function () {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/produit/recupererTout",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data;
            }, function (error) {
                console.error("error adding to cart", error);
                return error;
            });
        },

        getDetailsProduct: function (reference) {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/produit/detailsProduit/" + reference,
                headers: ApiService.getHeadersJson(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        getAllProductsAdmin: function () {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/produit/recupererToutAdmin",
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        getProductsWorker: function (worker_id) {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/produit/recupererProduitsArtisan?idArtisan=" + worker_id,
                headers: ApiService.getHeadersUrlencoded(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            }, function (error) {
                console.error("error adding to cart", error);
                return error;
            });
        },

        getImage: function (reference) {
            let req = {
                method: 'GET',
                url: ApiService.getBaseApiUrl() + "/api/produit/recupererImage/" + reference,
                headers: ApiService.getHeadersMultipartFormdata(userService.getToken())
            };

            return $http(req, {responseType: 'arraybuffer'}).then(function (data) {
                return data.data;
            });
        },

        deleteProduct: function (products) {
            let req = {
                method: 'DELETE',
                data: {
                    'referenceProduits': products
                },
                url: ApiService.getBaseApiUrl() + "/api/produit/supprimerProduit",
                headers: ApiService.getHeadersJson(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            }, function (error) {
                console.error("error delete product", error);
                return error;
            });
        },

        updateProduct: function (formdata) {
            return $http.put(ApiService.getBaseApiUrl() + "/api/produit/modifierProduitParArtisan", formdata, ApiService.getPayloadCTUndefined(userService.getToken()))
                .then(function (response) {
                    return response;
                });

        },

        updateProductAdmin: function (formdata) {
            return $http.put(ApiService.getBaseApiUrl() + "/api/produit/modifierAdmin", formdata, ApiService.getPayloadCTUndefined(userService.getToken()))
                .then(function (response) {
                    return response;
                });

        },

        createProduct: function (product) {

            return $http.post(ApiService.getBaseApiUrl() + "/api/produit/creer", product, ApiService.getPayloadCTUndefined(userService.getToken()))
                .then(function (response) {
                    return response;
                });
        },

        blockProduct: function (reason, refs) {
            let req = {
                method: 'PUT',
                data: {
                    'raison': reason,
                    'references': [...refs]
                },
                url: ApiService.getBaseApiUrl() + "/api/produit/bloquerProduits",
                headers: ApiService.getHeadersJson(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        },

        activateProduct: function (reason, refs) {
            let req = {
                method: 'PUT',
                data: {
                    'raison': reason,
                    'references': [...refs]
                },
                url: ApiService.getBaseApiUrl() + "/api/produit/autoriserProduits",
                headers: ApiService.getHeadersJson(userService.getToken())
            };

            return $http(req).then(function (data) {
                return data.data;
            });
        }
    };

}]);