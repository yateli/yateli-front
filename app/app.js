// Declare app level module which depends on views, and components
angular.module('yateliApp', [
    'ngRoute',
    'yateliApp.map',
    'yateliApp.connection',
    'yateliApp.passwordForgot',
    'yateliApp.products',
    'yateliApp.cart',
    'yateliApp.profile',
    'yateliApp.paid',
    'yateliApp.order',
    'yateliApp.whoweare',
    'yateliApp.subscribe',
    'yateliApp.activateAccount',
    'yateliApp.profileWorker',
    'yateliApp.profileAdmin',
    'yateliApp.productDetails',
    'ngMaterial',
    'ngMessages',
    'ngMdBadge',
    'ngStomp',
    'ngCookies',
    'ng-sweet-alert',
    'toaster'
]).
config(['$locationProvider', '$routeProvider', '$mdThemingProvider', function($locationProvider, $routeProvider, $mdThemingProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({redirectTo: '/map'});

    $mdThemingProvider.theme('default')
        .primaryPalette('brown')
        .accentPalette('amber');

}]).controller('ctrl', function ($scope, $cookies, $http, $mdToast, $timeout, userService, $stomp, SentenceService, ApiService, webSocketService,  $mdSidenav, NotificationService, ToasterService) {
    'use strict';
    let ctrl = this;
    ctrl.sidenavOpened = false;
    ctrl.connectedUser = {};

    ctrl.home_page = window.location.hash.includes("map");
    ctrl.connection_page = window.location.hash.includes("connection");
    ctrl.cart_page = window.location.hash.includes("cart");
    ctrl.profile_page = window.location.hash.includes("profile");
    ctrl.password = window.location.hash.includes("password");
    ctrl.activate_account = window.location.hash.includes("activerCompte");

    if($cookies.get("yateli_access_token")){
        let jsonToken = JSON.parse($cookies.get("yateli_access_token"));
        userService.setToken(jsonToken.access_token);
        userService.getUser().then(function (user) {
            webSocketService.initialize();
            NotificationService.initialize();
        }).catch(function () {
            if(!ctrl.connectedUser && (ctrl.profile_page || ctrl.cart_page)){
                window.location.href = "/#!/connection";
            }
        });
    }

    $scope.disconnectUser = function () {
        webSocketService.disconnect();
        userService.disconnectUser(true);
    };

    window.onhashchange = function () {
        ctrl.home_page = window.location.hash.includes("map");
        ctrl.connection_page = window.location.hash.includes("connection");
        ctrl.cart_page = window.location.hash.includes("cart");
        ctrl.profile_page = window.location.hash.includes("profile");
        ctrl.password = window.location.hash.includes("password");
        ctrl.activate_account = window.location.hash.includes("activerCompte");
        if(!ctrl.connectedUser && (ctrl.profile_page || ctrl.cart_page)){
            window.location.href = "/#!/connection";
        }
        setTimeout(function () {
            if (ctrl.sidenavOpened) {
                $scope.toggleSidenav();
            }
        }, 250);
    };

    $scope.$watch(function () {
        return userService.getUserObject();
    }, function (newVal, oldVal, scope) {
        ctrl.connectedUser = newVal;
    }, true);

    ctrl.panier = [];

    SentenceService.getSentence().then(function (data) {
        ctrl.sentence = data.sentence;
    });

    $scope.$watch(function () {
        return SentenceService.getSentenceService();
    }, function (newVal, oldVal) {
        if(newVal && newVal.sentence && newVal.sentence !== ctrl.sentence){
            ToasterService.success('Phrase du jour modifiée', 'La phrase du jour et l\'image de fond sont à jour !', 3000, true);
            ctrl.sentence = newVal.sentence;
        }
    }, true);

    $stomp.setDebug(function (args) {
        console.log(args);
    });

    $scope.toggleSidenav = function (componentId) {
        $mdSidenav(componentId).toggle();
    };

    $scope.onSwipeLeft = function(ev, target) {
        $scope.toggleSidenav();
    };

});
