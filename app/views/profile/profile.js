angular.module('yateliApp.profile', ['ngRoute', 'ngMaterial'])

/*   .run(function($rootScope, $interval) {
       $rootScope.time = 0;
       $interval(function() {
           $rootScope.time += 1000;
       }, 1000);
   }) */

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/profile', {
            templateUrl: 'views/profile/profile.html',
            css: 'views/profile/profile.css',
            controller: 'profileCtrl',
            controllerAs: "profileCtrl"
        });

    }])

    .controller('profileCtrl', function ($scope, $mdDialog, $interval, $timeout, userService, MysteryboxService, jeuConcoursService, FactureService, $cookies, CountryService, $mdToast) {
        'use strict';

        let self = this;

        self.endLoadingInvoices = false;
        self.titleInvoice = "VOS FACTURES";
        self.invoices = [];

        FactureService.getAllInvoices().then(function (data) {
            data.data.forEach(inv => {
                FactureService.getInvoice(inv.id).then(function (response) {
                    const dateFacture = new Date(inv.dateCreation);
                    inv.nom = "Facture" + dateFacture.getDate() + (dateFacture.getMonth()+1) + dateFacture.getFullYear() + ".pdf";
                    inv.file = new Blob([response], {type: 'application/pdf'});
                    self.invoices.push(inv);
                });
            });
            self.endLoadingInvoices = true;
        }).catch(function () {
            self.titleInvoice = "PAS DE FACTURE";
            self.endLoadingInvoices = true;
        });

        self.user = userService.getUserObject();

        if (!self.user) {
            userService.getUser().then(function (data) {
                if (data.status === 401) {
                    window.location.href = '/#!/connection';
                }
                self.typeErreur();
            }, function (error) {
                console.error('error fetching user', error);
            });
        }

        self.becomeWorker = function() {
            $mdDialog.show({
                controller: BecomeWorkerController,
                templateUrl: 'views/profile/become_worker.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose:true,
                fullscreen: true // Only for -xs, -sm breakpoints.
            })
                .then(function(answer) {
                }, function() {
                });


        };

        function BecomeWorkerController($scope, $element, CountryService, WorkerService, userService, $mdToast) {
            $scope.loading = true;
            $scope.vegetables = [];
            $scope.modelDemand = {};

            CountryService.getCountries().then(response => {
                $scope.vegetables = response.data;
                $scope.loading = false;
            });

            $scope.searchTerm = '';
            $scope.clearSearchTerm = function() {
                $scope.searchTerm = '';
            };


            $scope.closeDialog = function() {
                $mdDialog.hide();
            };

            $scope.demand = function(demand) {
                $scope.loading = true;
                demand.mail = userService.getUserObject()['mail'];
                let fm = new FormData();
                if (demand.image) {
                    fm.append('image', demand.image);
                    delete demand.image;
                }
                const w_dto = new Blob([JSON.stringify(demand)],{ type: "application/json"});
                fm.append('artisan', w_dto);
                WorkerService.workerDemand(fm).then(response => {
                    $scope.loading = false;
                    $scope.closeDialog();
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Demande créée, nous serez notifié quand elle sera traitée')
                            .position('bottom right')
                            .hideDelay(3000));
                });
            };

            $(function(ready){

                $('#browse-image').on('click', function () {
                    $("#picture-chooser-create").click();
                });

                $('#picture-chooser-create').change(function(){
                    let $this = this;
                    var fr = new FileReader();
                    fr.onload = function () {
                        $('#img-popup-create').attr('src', fr.result);
                        $scope.modelDemand.image = $($this)[0].files[0];
                    };
                    fr.readAsDataURL(this.files[0]);
                });

                // The md-select directive eats keydown events for some quick select
                // logic. Since we have a search input here, we don't need that logic.
                $element.find('input').on('keydown', function(ev) {
                    ev.stopPropagation();
                });
            });
        }

        $scope.downloadPdf = function (invoice) {
            const a = document.createElement("a");
            document.body.appendChild(a);
            a.href = window.URL.createObjectURL(invoice.file);
            a.download = invoice.nom;
            a.click();
        };

        self.subscribed = false;
        self.previousColisMystere = null;
        self.timeBeforeDiscover = 0;
        self.progessDate = null;
        self.timeBeforeDiscoverString = "";
        self.dateColisMystere = null;

        self.remainTime = false;
        self.erreurHistoriqueColis = "";

        self.jeuxConcoursListe = {};
        self.canOpenJeuConcoursDialog = false;

        let stop = function () {
            if (angular.isDefined(self.dateColisMystere)) {
                $interval.cancel(self.dateColisMystere);
            }
        };

        self.deleteUser = function() {
            userService.deleteUser().then(function () {
                $cookies.remove('yateli_access_token');
                window.location.reload();
            });
        };

        self.getMysteryBox = function () {
            self.remainTime = true;
            MysteryboxService.getMysteryBox().then(function (data) {
                let donneesProduits = {};
                donneesProduits[0] = data.data.produit.nom;
                donneesProduits[1] = data.data.produit.id;
                $mdDialog.show({
                    locals: {dataToPass: donneesProduits},
                    controller: DialogController,
                    templateUrl: 'views/profile/dialog-new-colis-mystere.html',
                    clickOutsideToClose: true,
                });
            }).catch(function (error) {
                if (error.data.exception === "DateDecouverteColisMystereException") {
                    self.subscribed = true;
                    self.getColisMystereDiscoverTime(error.data.message);
                } else {
                    self.subscribed = false;
                    self.timeBeforeDiscoverString = error.data.message;
                }
            });
        };

        $scope.$watch(function () {
            return userService.getUserObject();
        }, function (newVal, oldVal, scope) {
            if (newVal) {
                self.user = newVal;
            }
        }, true);

        self.typeErreur = function () {
            if (self.user) {
                if (!self.user.estAbonne) {
                    self.erreurHistoriqueColis = "Vous n'êtes pas abonné au colis mystère";
                } else {
                    if (self.user.colisMystereListe.length <= 0 || !self.user.colisMystereListe[0].produit) {
                        self.erreurHistoriqueColis = "Vous n'avez pas encore reçu de colis mystere";
                    }
                }
            }
        };

        self.typeErreur();

        self.fetchJeuxConcours = function () {
            jeuConcoursService.getAllJeuxConcours().then(function (data) {
                self.jeuxConcoursListe = data;
                self.canOpenJeuConcoursDialog = self.user.pointsFidelite > 0 && self.jeuxConcoursListe.length > 0;
            }).catch(function () {
                self.canOpenJeuConcoursDialog = false;
            });
        };

        self.fetchJeuxConcours();

        self.participateJeuConcours = function () {
            let donneesJeux = {};
            donneesJeux[0] = self.user;
            donneesJeux[1] = self.jeuxConcoursListe;
            $mdDialog.show({
                locals: {donneesJeux: donneesJeux},
                controller: JeuConcoursController,
                templateUrl: 'views/profile/dialog-jeu-concours.html',
                clickOutsideToClose: false,
            });
        };

        self.watchTimeRemaining = function () {
            self.getMysteryBox();
        };

        self.watchHistorique = function () {
            self.remainTime = false;
        };

        self.subscribeColisMystere = function () {
            userService.subscribeUser().then(function (data) {
                self.user = data;
                self.typeErreur();
                console.log("ABONNE");
            }).catch(function (data) {
                console.error(data);
            });
        };

        self.getLastColisMystere = function () {
            if (self.user.colisMystereListe.length - 2 >= 0 && self.user.colisMystereListe[self.user.colisMystereListe.length - 2].arecupereColis) {
                self.previousColisMystere = self.user.colisMystereListe[self.user.colisMystereListe.length - 2].dateDecouverte;
            }
        };

        self.getColisMystereDiscoverTime = function (timestampDecouverte) {
            self.timeBeforeDiscover = timestampDecouverte;
            self.getLastColisMystere();
            stop();
            self.dateColisMystere = $interval(function () {
                let now = new Date();

                if (self.previousColisMystere) {
                    self.progessDate = ((now.getTime() - self.previousColisMystere) / (self.timeBeforeDiscover - self.previousColisMystere)) * 100;
                } else {
                    let firstDay = new Date(now.getFullYear(), now.getMonth(), 1).getTime();
                    self.progessDate = ((now.getTime() - firstDay) / (self.timeBeforeDiscover - firstDay)) * 100;
                }

                if (self.progessDate > 100) {
                    stop();
                    self.progessDate = null;
                    self.getMysteryBox();
                }

                let distance = timestampDecouverte - now.getTime();

                let days = Math.floor(distance / (1000 * 60 * 60 * 24));
                let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                let seconds = Math.floor((distance % (1000 * 60)) / 1000);

                self.timeBeforeDiscoverString = days + "d " + hours + "h "+ minutes + "m " + seconds + "s ";
            }, 1000);
        };

        function DialogController($scope, $mdDialog, dataToPass) {
            $scope.nomProduit = dataToPass[0];
            $scope.idProduit = dataToPass[1];
            $scope.hide = function () {
                // $mdDialog.hide();
            };

            $scope.cancel = function () {
                // $mdDialog.cancel();
            };

            $scope.answer = function () {
                userService.getUser().then(function (data) {
                    self.user = data;
                    self.remainTime = false;
                    $mdDialog.hide();
                });
            };
        }

        self.showConfirm = function(ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            let confirm = $mdDialog.confirm()
                .title('ATTENTION')
                .textContent('Êtes-vous sûr de vouloir supprimer votre compte ?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('OUI')
                .cancel('Non j\'ai menti');

            $mdDialog.show(confirm).then(function() {
                // $scope.status = 'You decided to get rid of your debt.';
                self.deleteUser();
            }, function() {
                // $scope.status = 'You decided to keep your debt.';
            });
        };

        function JeuConcoursController($scope, $mdDialog, donneesJeux) {
            $scope.pointsFidelite = donneesJeux[0].pointsFidelite;
            $scope.jeuxConcours = donneesJeux[1];
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                // $mdDialog.cancel();
            };

            $scope.participate = function (id) {
                jeuConcoursService.participateJeuConcours(id).then(function (data) {
                    userService.getUser().then(function () {
                        $mdDialog.hide();
                    });
                }).catch(function (error) {
                    console.log(error.data.message);
                });
            };
        }

    });