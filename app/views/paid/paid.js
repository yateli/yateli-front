angular.module('yateliApp.paid', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/paid', {
            templateUrl: 'views/paid/paid.html',
            css: 'views/paid/paid.css',
            controller: 'paidCtrl',
            controllerAs: 'paidCtrl'
        });

    }])

    .controller('paidCtrl', function ($scope, $cookies, userService, PayementService, $q) {
        'use strict';

        let self = this;

        self.order_return = {
            "id": 0,
            "idCommande": 0,
            "montantTotal": 0,
            "userLogin": ""
        };

        self.promise = $q.defer();

        if($cookies.get("yateli_access_token")){
            let jsonToken = JSON.parse($cookies.get("yateli_access_token"));
            userService.setToken(jsonToken.access_token);
            userService.getUser().then(function () {

            }).catch(function () {
                if(!ctrl.connectedUser && (ctrl.profile_page || ctrl.cart_page)){
                    window.location.href = "/#!/connection";
                }
            });
        }

        if (window.location.hash.includes("?")) {
            self.order = parse_query_string(window.location.hash.split("?")[1]);

            delete self.order["token"];

            self.loading = true;

            PayementService.returnPaiement(self.order).then(function (data) {
                self.loading = false;
                self.order_return = data.data;
            });
        }




        function parse_query_string(query) {
            var vars = query.split("&");
            var query_string = {};
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                var key = decodeURIComponent(pair[0]);
                var value = decodeURIComponent(pair[1]);
                // If first entry with this name
                if (typeof query_string[key] === "undefined") {
                    query_string[key] = decodeURIComponent(value);
                    // If second entry with this name
                } else if (typeof query_string[key] === "string") {
                    query_string[key] = [query_string[key], decodeURIComponent(value)];
                    // If third or later entry with this name
                } else {
                    query_string[key].push(decodeURIComponent(value));
                }
            }
            return query_string;
        }


    });