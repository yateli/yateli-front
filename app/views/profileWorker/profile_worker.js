angular.module('yateliApp.profileWorker', ['ngRoute', 'ngCookies', 'ngFileUpload', 'angularFileUpload'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/profileWorker', {
            templateUrl: 'views/profileWorker/profile_worker.html',
            css: 'views/profileWorker/profile_worker.css',
            controller: 'profileWorkerCtrl',
            controllerAs: 'profileWorkerCtrl'
        });

    }])

    .controller('profileWorkerCtrl', function ($scope, $timeout, userService, $cookies, productService, $q, $mdDialog, WorkerService, $mdToast, $http, ApiService, tagService) {
        'use strict';

        const self = this;

        self.productList = [];

        self.worker = {};

        self.user = {};

        self.productsLoading = true;
        self.workerLoading = true;

        const qUser = $q.defer();

        userService.getUser().then(user => {
            self.user = user;
            WorkerService.getAllWorkers().then(workers => {
                console.log('workers', workers);
                let workerFound = workers.data.find(w => w.user.mail === user.mail);
                console.log('user found', workerFound);
                if (workerFound) {
                    WorkerService.getImage(workerFound.id).then(img => {
                        workerFound.img = img.data;
                        self.worker = workerFound;
                        console.log('worker', self.worker);
                        qUser.resolve(user);
                        self.workerLoading = false;
                    });
                }
            });

        });


        productService.getAllProducts().then(function (products) {
            const qTab = [];
            const tmp_list = [];
            qUser.promise.then(user => {
                products.data.forEach(prod => {
                    if (prod.artisan && prod.artisan.user.mail === user.mail) {
                        tmp_list.push(prod);
                    }
                });
                tmp_list.forEach(prod => {
                    const q = $q.defer();
                    qTab.push(q.promise);
                    productService.getImage(prod.reference).then(dataImg => {
                        prod.img = dataImg;
                        q.resolve(dataImg);
                    }, function (error) {
                        console.error(error);
                        q.reject(error);
                    });
                });
                $q.all(qTab).then(() => {
                    self.productList = tmp_list;
                    self.productsLoading = false;
                }, function (error) {
                    self.productList = tmp_list;
                    console.error(error);
                    self.productsLoading = false;
                });
            });

        });

        self.updateWorker = function(dataUpdate) {
            let data = {
                "mail": self.user.mail
            };
            if (dataUpdate.key === 'prenom') {
                data.prenom = dataUpdate.value;
                userService.updateUser(data).then(response => {
                    dataUpdate.loading = false;
                    dataUpdate.showEdit = false;
                    self.worker.user.prenom  = response.data.prenom;
                });
            } else {
                data[dataUpdate.key] = dataUpdate.value;
                WorkerService.updateWorker(data).then(response => {
                    dataUpdate.loading = false;
                    dataUpdate.showEdit = false;
                });
            }

        };

        self.deleteProduct = function(product, index) {

            var confirm = $mdDialog.confirm()
                .title('Would you like to delete this product?')
                .textContent('The product ' + product.nom + " will be deleted.")
                .ariaLabel('Delete question')
                .ok('DELETE')
                .cancel('CANCEL');

            $mdDialog.show(confirm).then(function() {
                productService.deleteProduct([product.reference]).then(function (response) {
                    console.log('response after delete', response);
                    if (!response.data.codeErreur) {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Produit supprimé')
                                .position("bottom right")
                                .hideDelay(3000));
                        self.productList.splice(index, 1);
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Erreur')
                                .position("bottom right")
                                .hideDelay(3000));
                    }

                });
            }, function() {

            });

        };


        self.createProduct = function() {
            var parentEl = angular.element(document.body);
            $mdDialog.show({
                parent: parentEl,
                templateUrl: 'views/profileWorker/createProduct.tmpl.html',
                controller: CreateProductController,
                fullscreen: true
            });


            function CreateProductController($scope, $mdDialog) {
                $scope.modelCreate = {};

                $scope.creatingProduct = false;

                $scope.closeDialog = function() {
                    $mdDialog.hide();
                };

                $scope.createProduct = function(product) {
                    $scope.creatingProduct = true;
                    let img = product.img;
                    delete product.img;
                    console.log('product', product);
                    product.mailArtisan = userService.getUserObject().mail;
                    let fm_data = new FormData();
                    if (img) {
                        fm_data.append('image', img);
                    }
                    fm_data.append('produit', new Blob([JSON.stringify(product)], { type: "application/json"}));
                    console.log('form data', fm_data);
                    productService.createProduct(fm_data).then(response => {
                        console.log('response after create', response);
                        $scope.creatingProduct = false;
                        $mdDialog.hide();
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Product created, you\'ll be able to see it when an admin will validate it')
                                .position('bottom right')
                                .hideDelay(3000));
                        /*let prod = response.data;
                        productService.getImage(prod.reference).then(image => {
                            prod.img = image;
                            self.productList.push(prod);
                        });*/
                    }, error => {
                        console.error('error', error);
                    });
                };

                $(function(ready){

                    $('#browse-image').on('click', function () {
                        $("#picture-chooser-create").click();
                    });

                    $('#picture-chooser-create').change(function(){
                        let $this = this;
                        console.log('this', this);
                        var fr = new FileReader();
                        fr.onload = function () {
                            $('#img-popup-create').attr('src', fr.result);
                            $scope.modelCreate.img = $($this)[0].files[0];
                        };
                        fr.readAsDataURL(this.files[0]);
                    });
                });
            }
        };


        self.editProduct = function(product) {
            var parentEl = angular.element(document.body);
            $mdDialog.show({
                parent: parentEl,
                templateUrl: 'views/profileWorker/editProduct.tmpl.html',
                locals: {
                    product: product
                },
                controller: EditProductController
            });

            function EditProductController($scope, $mdDialog, product) {
                $scope.product = product;
                $scope.modelUpdate = angular.copy(product);
                delete $scope.modelUpdate.img;

                $scope.loading = false;

                setTimeout(() => {
                    $('#img-popup').attr('src', 'data:image/png;base64, ' + product.img);
                });

                $scope.closeDialog = function() {
                    $mdDialog.hide();
                };

                $scope.updateProduct = function(product) {
                    $scope.loading  = true;
                    let toUpdate = {
                        'nom': product.nom,
                        'quantiteStock': product.quantiteStock,
                        'description': product.description,
                        'reference': product.reference,
                        'mailArtisan': product.artisan.user.mail,
                    };
                    let formdata = new FormData();
                    const produitDto = new Blob([JSON.stringify(toUpdate)],{ type: "application/json"});
                    if (product.img) {
                        formdata.append('image', product.img);
                    }
                    formdata.append('produit', produitDto);

                    productService.updateProduct(formdata).then(function (response) {
                        console.log('response after update', response);
                        let productUpdated = response.data;
                        productService.getImage(productUpdated.reference).then(dataImg => {
                            productUpdated.img = dataImg;
                            let index = self.productList.findIndex(p => p.reference === productUpdated.reference);
                            self.productList[index] = angular.copy(productUpdated);
                            $scope.closeDialog();
                            $scope.loading = false;
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Product updated')
                                    .position('bottom right')
                                    .hideDelay(3000)
                            );
                        });
                    });

                };

                $(function(){

                    $('#browse-image').on('click', function () {
                        $("#picture-chooser").click();
                    });

                    $('#picture-chooser').change(function(){
                        let _this = this;
                        var fr = new FileReader();
                        fr.onload = function () {
                            $('#img-popup').attr('src', fr.result);
                            $scope.modelUpdate.img = $(_this)[0].files[0];
                        };
                        $scope.modelUpdate.img = this.files[0];
                        fr.readAsDataURL(this.files[0]);
                    });
                });

            }
        };

    });