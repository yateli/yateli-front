'use strict';
angular.module('yateliApp.passwordForgot', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/password-forgot', {
            templateUrl: 'views/password-forgot/password-forgot.html',
            controller: 'passwordForgotCtrl',
            css: 'views/password-forgot/password-forgot.css'
        });

    }])

    .controller('passwordForgotCtrl', function ($scope, $timeout, userService) {

        let self = this;

        self.enCours = false;
        self.login = "";
        self.code = "";
        self.newPassword = "";
        self.confirmNewPassword = "";
        self.reinit = true;
        self.changePassword = false;

        self.renewAccount = function (login) {

            self.enCours = true;

            userService.passwordForgot(login).then(function (data) {
                if (data) {
                    self.reinit = false;
                    self.enCours = false;
                } else {
                    window.location.href = "/";
                }

            });

        };

        self.verifCode = function (code) {

            self.enCours = true;

            userService.verifCodePasswordForgot(self.login, code).then(function (data) {
                if(data){
                    self.changePassword = true;
                    self.enCours = false;
                }else{
                    //En attendant d'avoir la popup d'erreur
                    window.location.href = "/";
                }
            });

        };

        self.changePasswordUser = function (newPassword, confirmNewPassword) {

            self.enCours = true;

            if(newPassword === confirmNewPassword){
                userService.editPasswordWithCode(self.code, newPassword).then(function (data) {
                    if(data){
                        window.location.href = "#!/connection";
                    }
                });
            }else{
                //Error popup
            }

        }

    });