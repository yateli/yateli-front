(function (angular, mapboxgl, turf, undefined) {
    'use strict';

    angular.module('yateliApp.map', ['ngRoute', 'mapboxgl-directive', 'angularCSS'])

        .config(['interactiveLayersProvider', '$routeProvider', function (interactiveLayersProvider, $routeProvider) {
            interactiveLayersProvider.addLayer('background');

            $routeProvider.when('/map', {
                templateUrl: 'views/map/map.html',
                controller: 'mapCtrl',
                css: 'views/map/map.css'
            });
        }])

        .run([function () {
            mapboxgl.accessToken = 'pk.eyJ1Ijoic3RlcGhhbmVjb25xIiwiYSI6ImNqbjZmb2VhcjE5MDEza3J1YjN1bHJjNGcifQ.u7uapzcqyusnrzlXJvwHqQ';
        }])

        .controller('mapCtrl', ['$scope', '$window', '$timeout', 'mapboxglMapsData', '$compile', 'version','$log', '$q', 'WorkerService', 'productService', 'SentenceService',
            function ($scope, $window, $timeout, mapboxglMapsData, $compile, version, $log, $q, WorkerService, productService, SentenceService) {

                let self = this;

                $scope.$watch(function () {
                    return SentenceService.getSentenceService();
                }, function (newVal, oldVal) {
                    if(newVal){
                        document.getElementById("bg-image").style.backgroundImage = 'linear-gradient( rgba(0, 0, 0, 0.5) 100%, rgba(0, 0, 0, 0.5)100%),url(' + newVal.urlBackground + ')';
                        document.getElementById("bg-image").style.height = "100%";
                        document.getElementById("bg-image").style.backgroundPosition = "center";
                        document.getElementById("bg-image").style.backgroundRepeat = "no-repeat";
                        document.getElementById("bg-image").style.backgroundSize = "cover";
                    }
                }, true);

                self.glHeight = document.getElementsByClassName("map-container")[0].style.height;
                self.glWidth = document.getElementsByClassName("map-container")[0].style.width;

                self.glCenter = {
                    lat: 43.612729,
                    lng: 3.169441
                };

                self.glMarkers = [];

                WorkerService.getAllWorkers().then(function (workers) {

                    workers.data.forEach(function (art) {
                        self.autocomplete.push({
                            value: art.user.prenom.toLowerCase(),
                            display: art.user.prenom + " - Artisan"
                        });

                        createMarker(art).then(function (data) {
                            self.glMarkers.push(data);
                        });
                        
                        // self.glMarkers.push(createMarker(art, compiledHtml));

                    });
                });

                self.simulateQuery = false;
                self.isDisabled    = false;

                // list of `state` value/display objects
                // self.autocomplete        = loadAll();
                self.autocomplete = [];
                self.querySearch   = querySearch;
                self.selectedItemChange = selectedItemChange;
                self.searchTextChange   = searchTextChange;

                let popup_template = '<div layout="row">' +
                    '<div layout="column" flex="40" layout-align="center center">' +
                    '<div flex="40" layout="row" layout-align="center center"><h3 flex><!name!></h3></div>' +
                    '<div flex="60" layout="row" layout-align="center center"><img src="<!img!>" class="img-artisan"></div>' +
                    '</div>' +
                    '<div flex="60" layout="column" layout-align="center center">' +
                    '<div flex="90"><!description!></div>' +
                    '<div flex="10" layout="row" layout-align="center center"><a flex href="/#!/products?id=<!id!>">Voir les produits</a></div>' +
                    '</div>' +
                    '</div>';

                productService.getAllProducts().then(function (products) {
                    products.data.forEach(prod => {
                        self.autocomplete.push({
                            value: prod.nom.toLowerCase(),
                            display: prod.nom + " - Produit"
                        });
                    });
                });


                function createElement (photo) {
                    let element = document.createElement('div');
                    if (photo) {
                        element.style.backgroundImage = 'url(data:image/png;base64,' + photo + ')';
                    } else {
                        element.style.backgroundImage = 'url(https://placekitten.com/g/50/50)';
                    }
                    element.style.width = '40px';
                    element.style.height = '40px';
                    element.style.borderRadius = '50%';
                    element.style.backgroundSize = 'cover';
                    element.style.backgroundRepeat = 'no-repeat';
                    return element;
                }


                // ******************************
                // Internal methods
                // ******************************

                /**
                 * Search for autocomplete... use $timeout to simulate
                 * remote dataservice call.
                 */
                function querySearch (query) {
                    var results = query ? self.autocomplete.filter( createFilterFor(query) ) : self.autocomplete,
                        deferred;
                    if (self.simulateQuery) {
                        deferred = $q.defer();
                        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
                        return deferred.promise;
                    } else {
                        return results;
                    }
                }

                function searchTextChange(text) {
                    $log.info('Text changed to ' + text);
                }

                function selectedItemChange(item) {
                    $log.info('Item changed to ' + JSON.stringify(item));
                }

                /**
                 * Create filter function for a query string
                 */
                function createFilterFor(query) {
                    var lowercaseQuery = query.toLowerCase();

                    return function filterFn(state) {
                        return (state.value.indexOf(lowercaseQuery) === 0);
                    };

                }


                $window.onresize = function (event) {
                    $scope.$apply(function () {
                        $scope.glHeight = event.target.innerHeight;
                        $scope.glWidth = event.target.innerWidth;
                    });
                };


                $scope.glStyle = 'mapbox://styles/valentincpe/cjolrth7u0jpi2squ0n1p5uon';

                function createMarker(art) {
                    let promise = $q.defer();
                    let coordinates = [
                        art.emplacement.longitude,
                        art.emplacement.latitude
                    ];

                    let html_popup = angular.copy(popup_template);

                    WorkerService.getImage(art.id).then(function (data) {
                        let element = null;
                        let compiledHtml = null;

                        if(data.status !== 200){
                            element = createElement(false);
                            html_popup = html_popup.replace("<!name!>", art.user.prenom).replace("<!img!>", "").replace("<!description!>", art.nationalite).replace("<!id!>", art.id);
                        }else{
                            element = createElement(data.data);
                            html_popup = html_popup.replace("<!name!>", art.user.prenom).replace("<!img!>", "data:image/png;base64,"+data.data).replace("<!description!>", art.nationalite).replace("<!id!>", art.id);
                        }

                        compiledHtml = $compile(html_popup)($scope);

                        promise.resolve({
                            coordinates: coordinates,
                            element: element,
                            options: {
                                offset: [-20, -20]
                            },
                            popup: {
                                enabled: true,
                                message: compiledHtml,
                                options: {
                                    offset: 20
                                }
                            }
                        });
                    });
                    return promise.promise;
                }
                
                self.goTo = function (page) {
                    window.location = '/#!/' + page;
                };


            }]);
})(window.angular, window.mapboxgl, window.turf);
