'use strict';
angular.module('yateliApp.subscribe', ['ngRoute','ngMaterial'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/subscribe', {
            templateUrl: 'views/subscribe/subscribe.html',
            css: 'views/subscribe/subscribe.css',
            controller: 'subscribeCtrl',
            controllerAs: 'subscribeCtrl'
        });

    }])

    .controller('subscribeCtrl', function ($scope, $timeout, userService, tagService, interrogationService, $mdDialog, ToasterService) {

        let self = this;
        self.user = {prenom: null, mail: null, telephone: null, password: null, role: null, reponses: [], confirmPassword: null};

        self.textButtonSurvey = "Remplir un questionnaire";

        self.firstTimeSurveyEmpty = true;

        self.fillInSurvey = function() {
            let donneesUser = {};
            donneesUser[0] = self.user.prenom;

            $mdDialog.show({
                locals: {dataToPass: donneesUser},
                controller: SurveyController,
                templateUrl: 'views/subscribe/dialog-survey.html',
                clickOutsideToClose: false,
            });
        };

        self.subscribe = function (user) {
            self.subscribing = true;
            user.role = "USER";
            Object.keys(user).forEach(function(key) {
                //console.log(key + ': ' + user[key]);
                if(user[key]===null && key !== 'telephone'){
                    console.warn("key is null");
                    self.erreur = "Les champs obligatoires doivent être remplis";
                    self.subscribing = false;
                    return;
                }
            });


            if(self.subscribing===false){
                self.erreur = "Les champs obligatoires doivent être remplis";
                return;
            }

            //console.log(' 11111 : ' + user.password);
            if(user.password!==user.confirmPassword){
                self.erreur = "Les mots de passe sont differents";
                self.subscribing = false;
                console.error('Les mots de passe sont differents');
                return;
            }

            if(user.reponses.length === 0){
                if(self.firstTimeSurveyEmpty){
                    ToasterService.warning('Questionnaire vide', 'Aucune de vos préférences ne sera pris en compte', 8000, true);
                    self.firstTimeSurveyEmpty = false;
                    self.subscribing = false;
                    return;
                }
            }

            //console.log('phone : '+user.telephone.charAt(0));
            if(user.telephone !== null && user.telephone !== ""){
                if(user.telephone.substr(0, 1)==='0'){
                    user.telephone = '+33'+user.telephone.substr(1);
                }else{
                    self.subscribing = false;
                    self.erreur = "numéro de téléphone erroné";
                    return;
                }
            }
            console.log(user);
            self.erreur = "else";

            delete user.confirmPassword;

            userService.createAccount(user).then(function (token) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#subscribe-container')))
                        .clickOutsideToClose(true)
                        .title('Merci !')
                        .textContent('Un mail de confirmation vous a été envoyé. Merci de confirmer votre inscription')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('OK')
                ).then(function () {
                    window.location = "/#!/map";
                });
            });

        };

        function SurveyController($scope, $mdDialog, dataToPass) {
            $scope.prenomUser = dataToPass[0];
            $scope.answers = [];

            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.sendSurvey = function () {
                if($scope.answers.length > 0){
                    self.textButtonSurvey = 'Questionnaire complété';
                    ToasterService.success('Merci', 'Vous avez complété votre formulaire', 6000, true);
                }

                self.user.reponses = Object.keys($scope.answers);
                $scope.hide();
            };

            interrogationService.getQuestions().then(function (data) {
                $scope.questions = data;
            }).catch(function () {
                ToasterService.error('Erreur', 'Erreur lors de la récupération des questions', 6000, true);
                $scope.hide();
            });

        }

    });