angular.module('yateliApp.connection', ['ngRoute', 'ngCookies'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/connection', {
            templateUrl: 'views/connection/connection.html',
            controller: 'connectionCtrl',
            css: 'views/connection/connection.css'
        });

    }])

    .controller('connectionCtrl', function ($scope, $timeout, userService, webSocketService, NotificationService, $mdDialog) {
        'use strict';
        let self = this;

        self.erreur = "";
        self.connecting = false;

        self.connect = function (email, password) {

            if(!password){
                self.erreur = "Les champs email et mot de passe doivent être remplis";
                return;
            }

            if (!email) {
                self.erreur = "Merci d'entrer une adresse email valide";
                return;
            }

            self.connecting = true;

            userService.connectUser(email, password).then(function (token) {
                self.erreur = "";
                userService.getUser().then(function (user) {
                    self.connecting = false;
                    webSocketService.initialize();
                    NotificationService.initialize();
                    window.location.href = "/#!/map";
                });
            }, function (err) {
                self.connecting = false;
                let message = '';
                if (err.status === 400) {
                    if(err.data.error_description.indexOf("lock") !== -1){
                        message = 'Votre compte est bloqué.';
                    }else{
                        message = 'Votre identifiant ou votre mot de passe sont faux, merci de les vérifier.';
                    }
                } else if (err.status === 401) {
                    message = 'Adresse email inconnue. Avez-vous un compte avec cette adresse ?';
                } else {
                    message = 'Une erreur inattendue est survenue. Merci d\'essayer plus tard ou de nous contacter si le problème persiste.';
                }
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('body')))
                        .clickOutsideToClose(true)
                        .title('Erreur de connexion')
                        .textContent(message)
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Mince alors...')
                );
            });

        };

        $(window).keydown(function(event){
            if(event.keyCode === 13) {
                self.connect($scope.email, $scope.password)
            }
        });


    });