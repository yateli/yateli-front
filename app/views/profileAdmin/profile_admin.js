angular.module('yateliApp.profileAdmin', ['ngRoute', 'ngCookies', 'ngMaterial'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/profileAdmin', {
            templateUrl: 'views/profileAdmin/profile_admin.html',
            css: 'views/profileAdmin/profile_admin.css',
            controller: 'profileAdminCtrl',
            controllerAs: 'profileAdminCtrl'
        });

    }])

    .controller('profileAdminCtrl', function ($scope, $timeout, userService, $cookies, productService, $q, $mdDialog, WorkerService, $mdToast, statistiquesService, ToasterService, tagService, interrogationService) {
        'use strict';

        const self = this;

        self.general_loading_workers = false;
        self.general_loading_products = false;

        self.validated_workers = [];
        self.workers_to_validate = [];
        self.blocked_workers = [];

        self.vw_loading = true;
        self.vw_validate_loading = true;
        self.active_products_loading = true;

        self.active_v_worker = null;
        self.active_products = [];

        self.all_products = {};

        self.questions = [];

        self.active_tab = 0;

        self.reportInfo = {
            'fileName': 'Rapport_Yateli'
        };

        self.beginDateExcel = 0;
        self.loginExcel = "";
        self.fileName = "test";
        self.fileNameFinal = "";

        self.fileGenerating = false;
        self.displayDlButton = false;

        WorkerService.getAllWorkers().then(function (response) {

            productService.getAllProductsAdmin().then(products => {
                products.forEach(p => {
                    if (p.artisan === null) {
                        console.warn('artisan is null', p);
                        return;
                    }
                    productService.getImage(p.reference).then(image => {
                        p.image = image;
                        pushProduct(p);
                    }, error => {
                        console.warn('no image for ', p.reference);
                        p.image = '';
                        pushProduct(p);
                    });

                });
            });

            let qTab = [];

            let workers = response.data;

            let blocked_workers = [];
            let valid_workers = [];

            response.data.forEach(w => {
                let q = $q.defer();
                qTab.push(q.promise);
                WorkerService.getImage(w.id).then(image => {
                    w.image = image.data;
                    if (isBlocked(w)) {
                        blocked_workers.push(w);
                    } else {
                        valid_workers.push(w);
                    }
                    q.resolve(w);
                });
            });

            $q.all(qTab).then(() => {
                self.validated_workers = [...valid_workers];
                self.blocked_workers = [...blocked_workers];
                self.vw_loading = false;
            });
        });

        WorkerService.getAllWorkersToAccept().then(workers => {
            let qGlobal = $q.defer();
            let qTab = [];
            workers.forEach(w => {
                let q = $q.defer();
                qTab.push(q.promise);
                WorkerService.getImage(w.id).then(response => {
                    w.image = response.data;
                    q.resolve(w.id);
                });
            });
            $q.all(qTab).then(() => {
                qGlobal.resolve(workers);
            });

            qGlobal.promise.then(workers => {
                self.workers_to_validate = [...workers];
                self.vw_validate_loading = false;
            });
        });

        interrogationService.getToutesQuestion().then(questions => {
            self.questions = questions;
        });

        self.activateQuestion = function(id) {
            interrogationService.activateQuest(id).then(response => {
                let q = self.questions.find(q => q.id === id);
                if (q) {
                    q.active = true;
                }
            });
        };

        self.deactivateQuestion = function(id) {
            interrogationService.deActivateQuest(id).then(response => {
                let q = self.questions.find(q => q.id === id);
                if (q) {
                    q.active = false;
                }
            });
        };

        self.createQuestion = function() {
            $mdDialog.show({
                controller: QuestController,
                controllerAs: 'QuestController',
                templateUrl: 'views/profileAdmin/edit_questions.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: true
            });

            function QuestController($scope, $mdDialog) {

                $scope.quest = {
                    "question": "",
                    "reponses": [
                        {
                            "tags": [],
                            "texte": ""
                        }
                    ]
                };

                $scope.addTag = function() {
                    $scope.quest.reponses.push({
                        "tags": [],
                        "texte": ""
                    });
                };

                $scope.createQuestion = function(quest) {
                    quest.reponses.forEach(r => {
                        let tmp = [];
                        r.tags.forEach(t => {
                            tmp.push({
                                'nom': t
                            });
                        });
                        r.tags = tmp;
                    });
                    interrogationService.createQuest(quest).then(question => {
                        console.log('response after create quest', question);
                        self.questions.push(question);
                        $scope.hide();
                    });
                };

                $scope.editQuestion = true;
                $scope.tags = [];
                $scope.searchTerm = '';
                $scope.clearSearchTerm = function() {
                    $scope.searchTerm = '';
                };

                $scope.tags = [];

                tagService.getAllTags().then(response => {
                    $scope.tags = [...response.data];
                    $scope.editLoading = false;
                }, error => {
                    $scope.editLoading = false;
                    console.error('error', error);
                    handleErrorFront('error fetching tags, please see logs');
                });

                $scope.hide = function() {
                    $mdDialog.hide();
                };

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.answer = function(answer) {
                    $mdDialog.hide(answer);
                };
            }
        };

        self.generateExcel = function(infos){
            self.fileGenerating = true;
            self.displayDlButton = false;
            if (infos.fileName) {
                self.fileName = infos.fileName;
            }
            if (infos.date) {
                self.beginDateExcel = new Date(infos.date).getTime();
            }
            if (infos.email) {
                self.loginExcel = infos.email;
            }
            self.fileNameFinal = self.fileName + ".xlsx";
            let toast = ToasterService.wait('Demande Excel effectuée', 'Votre fichier sera disponible d\'ici peu de temps', 0, true);
            statistiquesService.generateExcel(self.beginDateExcel, self.loginExcel, self.fileName).then(function (fileEncoded) {
                ToasterService.clear(toast);
//                ToasterService.success('Fichier généré', 'Fichier disponible au téléchargement rubrique "Reporting" : ' + self.fileNameFinal, 8000, true);
                let file = new Blob([fileEncoded], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                self.fileGenerating = false;
                self.displayDlButton = true;

                let link = window.URL.createObjectURL(file);
                let html = '<a href=\"' + link + '\" download=\"' + self.fileName + '.xlsx\">Cliquez pour télécharger le fichier</a>';
                ToasterService.html('success','Fichier généré', html, 0, true);
            }).catch(function (error) {
                console.error('error', error);
                let messageErreur = error.data.message;
                ToasterService.clear(toast);
                ToasterService.error('Erreur génération', messageErreur, 8000, true);
            });
        };

        function isBlocked(w) {
            if (w.blocages.length > 0) {
                if (w.blocages[w.blocages.length -1].actif) {
                    return true;
                }
            }
            return false;
        }

        function pushProduct(p) {
            if (!self.all_products[p.artisan.id]) {
                self.all_products[p.artisan.id] = {
                    'products': [],
                    'blocked': []
                };
            }
            if (p.blocages.length > 0) {
                if (p.blocages[p.blocages.length -1].actif) {
                    self.all_products[p.artisan.id].blocked.push(p);
                    return;
                }
            }
            self.all_products[p.artisan.id].products.push(p);
        }

        self.acceptWorker = function(login) {
            self.general_loading_workers = true;
            WorkerService.acceptWorker(login).then(response => {
                let index = self.workers_to_validate.findIndex(w => w.user.mail === login);
                if (index > -1) {
                    self.workers_to_validate.splice(index, 1);
                }
                self.general_loading_workers = false;
            }, function (error) {
                console.error('error', error);
                self.general_loading_workers = false;
                handleErrorFront('Error accepting worker, please check logs');
            });
        };

        self.authorizeWorker = function(w_id) {
            let confirm = $mdDialog.confirm()
                .title('Êtes-vous sûr(e) de vouloir autoriser cet artisan ?')
                .textContent('Il sera de nouveau visible sur le site.')
                .ok('Autoriser')
                .cancel('J\'AI MENTI');

            $mdDialog.show(confirm).then(function() {
                self.general_loading_workers = true;
                WorkerService.authorizeWorker(w_id).then(function (response) {
                    let index = self.validated_workers.findIndex(w => w.id === w_id);
                    if (index > -1) {
                        self.blocked_workers.push(self.validated_workers[index]);
                        self.validated_workers.splice(index, 1);
                    }
                    self.general_loading_workers = false;
                    console.log('response after autorized', response);
                }, error => {
                    console.error('error', error);
                    self.general_loading_workers = false;
                    handleErrorFront('Error authorizing worker, please check logs');
                });
            }, function() {

            });
        };

        self.blockWorker = function(w_id) {
            let confirm = $mdDialog.prompt()
                .title('Êtes-vous sûr(e) de vouloir bloquer cet artisan ?')
                .textContent('Si oui, merci de spécifier une raison.')
                .placeholder('Raison')
                .ariaLabel('Raison')
                .initialValue('')
                .required(true)
                .ok('REFUSER')
                .cancel('J\'ai menti');

            $mdDialog.show(confirm).then(function(result) {
                self.general_loading_workers = true;
                WorkerService.blockWorker(w_id, result).then(function (response) {
                    let index = self.validated_workers.findIndex(w => w.id === w_id);
                    if (index > -1) {
                        self.blocked_workers.push(self.validated_workers[index]);
                        self.validated_workers.splice(index, 1);
                    }
                    self.general_loading_workers = false;
                    console.log('response after blocked', response);
                }, error => {
                    console.error('error', error);
                    self.general_loading_workers = false;
                    handleErrorFront('Error blocking worker, please check logs');
                });
            });
        };

        self.deleteWorker = function(mail) {
            let confirm = $mdDialog.confirm()
                .title('Êtes-vous sûr de supprimer cet utilisateur ?')
                .textContent('')
                .ok('SUPPRIMER')
                .cancel('J\'ai menti');

            $mdDialog.show(confirm).then(function() {
                self.general_loading_workers = true;
                userService.deleteSpecificUser(mail).then(response => {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Utilisateur supprimé')
                            .position('bottom right')
                            .hideDelay(3000));
                    let index = self.blocked_workers.findIndex(w => w.user.mail === mail);
                    if (index > -1) {
                        self.blocked_workers.splice(index, 1);
                    }
                    self.general_loading_workers = false;
                }, error => {
                    console.error('error', error);
                    self.general_loading_workers = false;
                    handleErrorFront('Error deleting worker, please check logs');
                });
            });
        };

        self.declineWorker = function(login) {
            let confirm = $mdDialog.prompt()
                .title('Êtes-vous sûr(e) de vouloir refuser ' + login)
                .textContent('Si oui, merci de spécifier une raison.')
                .placeholder('Raison')
                .ariaLabel('Raison')
                .initialValue('')
                .required(true)
                .ok('REFUSER')
                .cancel('J\'ai menti');

            $mdDialog.show(confirm).then(function(result) {
                self.general_loading_workers = true;
                WorkerService.declineWorker(login, result).then(response => {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Demande refusée')
                            .position('bottom right')
                            .hideDelay(3000));
                    let index = self.workers_to_validate.findIndex(w => w.user.mail === login);
                    if (index > -1) {
                        self.workers_to_validate.splice(index, 1);
                    }
                    self.general_loading_workers = false;
                }, error => {
                    console.error('error', error);
                    self.general_loading_workers = false;
                    handleErrorFront('Error declining worker, please check logs');
                });
            });
        };

        self.blockProducts = function(ref, index, w_id) {
            let confirm = $mdDialog.prompt()
                .title('Êtes-vous sûr(e) de vouloir bloquer ce produit ?')
                .textContent('Si oui, merci de spécifier une raison.')
                .placeholder('Raison')
                .ariaLabel('Raison')
                .initialValue('')
                .required(true)
                .ok('BLOQUER')
                .cancel('J\'ai menti');

            $mdDialog.show(confirm).then(function(result) {
                self.general_loading_products = true;
                productService.blockProduct(result, [ref]).then(function (response) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Produit bloqué')
                            .position('bottom right')
                            .hideDelay(3000));
                    self.all_products[w_id].blocked.push(self.all_products[w_id].products[index]);
                    $timeout(() => {
                        self.all_products[w_id].products.splice(index, 1);
                    });
                    self.general_loading_products = false;
                }, function (error) {
                    console.error('error', error);
                    self.general_loading_products = false;
                    handleErrorFront('Error blocking product, please check logs');
                });
            }, function() {

            });
        };

        self.validateProduct = function(ref, index, w_id) {
            let confirm = $mdDialog.prompt()
                .title('Êtes-vous sûr(e) de vouloir débloquer ce produit ?')
                .textContent('Si oui, merci de spécifier une raison.')
                .placeholder('Raison')
                .ariaLabel('Raison')
                .initialValue('')
                .required(true)
                .ok('DÉBLOQUER')
                .cancel('J\'ai menti');

            $mdDialog.show(confirm).then(function(result) {
                self.general_loading_products = true;
                productService.activateProduct(result, [ref]).then(function (response) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Produit activé')
                            .position('bottom right')
                            .hideDelay(3000));
                    self.all_products[w_id].products.push(self.all_products[w_id].blocked[index]);
                    $timeout(() => {
                        self.all_products[w_id].blocked.splice(index, 1);
                        self.general_loading_products = false;
                    });

                }, function (error) {
                    console.error('error', error);
                    self.general_loading_products = false;
                    handleErrorFront('Error validating product, please check logs');
                });
            }, function() {

            });
        };

        self.deleteProduct = function(ref, index, w_id) {
            self.general_loading_products = true;
            var confirm = $mdDialog.confirm()
                .title('Êtes-vous sûr(e) de vouloir supprimer ce produit ?')
                .textContent('Il sera définitivement supprimé du site.')
                .ok('SUPPRIMER')
                .cancel('J\'AI MENTI');

            $mdDialog.show(confirm).then(function() {
                productService.deleteProduct([ref]).then(response => {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Produit supprimé')
                            .position('bottom right')
                            .hideDelay(3000));
                    self.active_products.blocked.splice(index, 1);
                    self.general_loading_products = false;
                }, error => {
                    console.error('error', error);
                    self.general_loading_products = false;
                    handleErrorFront('Error deleting product, please check logs');
                });
            }, function() {

            });
        };

        self.editProduct = function(p) {
            $mdDialog.show({
                controller: EditProductController,
                templateUrl: 'views/profileAdmin/edit_product.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose:true
            }).then(function(answer) {
            }, function() {
            });

            function EditProductController($scope) {

                $scope.product = angular.copy(p);

                $scope.editLoading = true;
                let tags_temp = [];
                p.tags.forEach(t => tags_temp.push(t.nom));

                $scope.productModel = {
                    "reference": p.reference,
                    "nom": p.nom,
                    "description": p.description,
                    "nomsTag": tags_temp,
                    "prixHT": p.prixHT,
                    "tauxTaxe": p.tauxTaxe,
                    "poids": p.poids,
                    "pointsFidelite": p.pointsFidelite,
                    "quantiteStock": p.quantiteStock,
                    "quantiteMinimum": p.quantiteMinimum
                };

                $scope.tags = [];

                tagService.getAllTags().then(response => {
                    $scope.tags = [...response.data];
                    $scope.editLoading = false;
                }, error => {
                    $scope.editLoading = false;
                    console.error('error', error);
                    handleErrorFront('error fetching tags, please see logs');
                });

                $scope.updateProduct = function(model) {
                    let img = model.image;
                    delete model.image;
                    let fm = new FormData();

                    if (img) {
                        fm.append('image', img);
                    }

                    model.mailArtisan = $scope.product.artisan.user.mail;

                    const produitDto = new Blob([JSON.stringify(model)],{ type: "application/json"});

                    fm.append('produit', produitDto);

                    productService.updateProductAdmin(fm).then(response => {
                        console.log('response', response);
                        let index = self.all_products[p.artisan.id].blocked.findIndex(pr => pr.id === p.id);
                        if (index > -1) {
                            self.all_products[p.artisan.id].blocked[index] = angular.copy(response.data);
                        }
                        $scope.cancel();
                    }, error => {
                        console.error('error', error);
                        handleErrorFront('Error updating product, please see logs');
                    });
                };

                $scope.tags = [];
                $scope.searchTerm = '';
                $scope.clearSearchTerm = function() {
                    $scope.searchTerm = '';
                };

                $(function(ready){

                    $('input').on('keydown', function(ev) {
                        ev.stopPropagation();
                    });

                    setTimeout(() => {
                        $('#img-popup-create').attr('src', 'data:image/png;base64, ' + p.image);
                    });

                    $('#browse-image').on('click', function () {
                        $("#picture-chooser-create").click();
                    });

                    $('#picture-chooser-create').change(function(){
                        let _this = this;
                        var fr = new FileReader();
                        fr.onload = function () {
                            $('#img-popup-create').attr('src', fr.result);
                            $scope.productModel.image = $(_this)[0].files[0];
                        };
                        $scope.productModel.image = this.files[0];
                        fr.readAsDataURL(this.files[0]);
                    });
                });

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
            }
        };

        self.setActiveW = function(w) {
            self.active_products_loading = true;
            self.active_products = [];
            self.active_v_worker = w;
            displayMapAt(w.emplacement.latitude, w.emplacement.longitude, 10);
            self.active_products = self.all_products[w.id];
            self.active_products_loading = false;
        };

        self.tabChangeEvent = function(tab_id) {
            self.active_tab = tab_id;
            self.active_v_worker = null;
        };

        function handleErrorFront(message) {
            $mdToast.show({
                hideDelay: 3000,
                position: 'bottom right',
                controller: ToastCtrl,
                controllerAs: 'ctrl',
                bindToController: true,
                locals: {toastMessage: message},
                templateUrl: 'views/profileAdmin/errorToast.tmpl.html'
            });

            function ToastCtrl($mdToast, $mdDialog, $document, $scope) {

            }
        }

        function displayMapAt(lat, lon, zoom) {
            $("#map-container")
                .html(
                    "<iframe id=\"map_frame\" " +
                    "width=\"80%\" height=\"400px\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" " +
                    "src=\"https://www.google.com/maps?f=q&amp;q="+lat+","+lon+"&amp;output=embed&amp;hl=fr&amp;sspn=4.418559,10.821533&amp;ie=UTF8&amp;ll=" +
                    lat + "," + lon +
                    "&amp;z=" +
                    zoom + "\"" + "></iframe>");

        }

    });