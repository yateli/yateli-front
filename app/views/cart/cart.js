angular.module('yateliApp.cart', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/cart', {
            templateUrl: 'views/cart/cart.html',
            css: 'views/cart/cart.css',
            controller: 'cartCtrl',
            controllerAs: 'cartCtrl'
        });

    }])

    .controller('cartCtrl', function ($scope, userService, cartService, productService) {
        'use strict';

        let self = this;

        self.price = 0;

        self.cart = userService.getCart();

        if (self.cart === null) {
            window.location.href = "/";
        }

        self.updateQty = function(product, quantity) {
            product.quantite = quantity;
            cartService.updateQty(product).then(function (data) {
                userService.setCart(data.data);
            });
        };

        $scope.$watch(function () {
            return userService.getUserObject();
        }, function (newVal, oldVal, scope) {
            if (newVal) {
                self.cart = newVal.panier;
                self.calculatePrice();
            }
        }, true);

        self.removeFromCart = function (item) {
            cartService.deleteFromCart(item).then(function (response) {
                // userService.updateQty(item.id, item.quantite);
                userService.setCart(response.data);
            });

        };

        self.reduceQty = function(item) {
            cartService.deleteFromCart(item).then(function (response) {
                userService.removeFromCart();
                console.log("reponse", response);
            });
        };

        self.addQty = function(item) {
            let body = {
                reference: item.produit.reference,
                quantite: item.quantite
            };
            cartService.addToCart(body).then(function (response) {
                userService.updateQty(item.id, item.quantite);
            });
        };

        self.calculatePrice = function () {
            self.price = 0;
            if (self.cart && self.cart.produits) {
                self.cart.produits.forEach(function (product) {
                    self.price += product.produit.prixTTC * product.quantite;
                    productService.getImage(product.produit.reference).then(function (image) {
                        product.imageProduct = image;
                    }).catch(function (error) {
                        console.error(error.data.message);
                    });
                });
            }
        };

        self.calculatePrice();

    });