
angular.module('yateliApp.order', ['ngRoute', 'ngCookies', 'ngMaterial', 'ngMessages'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/order', {
            templateUrl: 'views/order/order.html',
            css: 'views/order/order.css',
            controller: 'orderCtrl',
            controllerAs: 'orderCtrl'
        });

    }])

    .controller('orderCtrl', function ($scope, $timeout, userService, $cookies, OrderService, PayementService, CountryService) {
        'use strict';
        let self = this;

        self.user = {};

        self.loading = false;

        self.reducAdded = [];

        self.countries = [];

        self.delivery = {
            "adresseLivraison": {
                "codePostal": "",
                "complementDestinataire": "",
                "complementPointGeographique": "",
                "identification": "",
                "numeroEtLibelleVoie": "",
                "pays": "",
                "serviceParticulier": "",
                "ville": ""
            },
            "codesBonsAchat": [
            ],
            "societeLivraison": "CHRONOPOST"
        };

        CountryService.getCountries().then(data => {
            self.countries = data.data;
        });


        $scope.$watch(function () {
            return userService.getUserObject();
        }, function (newVal) {
            self.user = newVal;
        });

        self.order = function (delivery) {
            self.loading = true;
            self.reducAdded.forEach(reduc => {
                delivery.codesBonsAchat.push(reduc.code);
            });
            OrderService.order(delivery).then(function (response) {
                if (response.status === 200) {
                    PayementService.payOrder(response.data.id).then(function (response) {
                        if (response.status === 200) {
                            window.location.href = response.data.message;
                        }
                    });
                }
            });
        };

        self.addReduc = function (reduc) {
            let index = self.user.bonAchats.findIndex(bon => {
                return bon.code === reduc.code;
            });
            if (index > -1) {
                self.selectedReduction = {};
                self.reducAdded.push(reduc);
                self.user.bonAchats.splice(index, 1);
            }
        };

        self.deleteReduc = function (reduc, index) {
            if (index > -1) {
                self.reducAdded.splice(index, 1);
                self.user.bonAchats.push(reduc);
            }
        };


        self.calculateTotal = function () {
            if (self.user && self.user.panier) {
                let total = self.user.panier.prixTTCTotal;
                self.reducAdded.forEach(reduc => {
                    total -= reduc.valeur;
                });
                return total;
            } else {
                return 0;
            }
        };

        $scope.searchTerm;
        $scope.clearSearchTerm = function() {
            $scope.searchTerm = '';
        };
        // The md-select directive eats keydown events for some quick select
        // logic. Since we have a search input here, we don't need that logic.
        document.getElementById('searchCountry').addEventListener('keydown', function(ev) {
            ev.stopPropagation();
        });

    });