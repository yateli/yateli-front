
angular.module('yateliApp.productDetails', ['ngRoute', 'ngMaterial', 'ngMessages'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/product', {
            templateUrl: 'views/product/product.html',
            css: 'views/product/product.css',
            controller: 'productCtrl',
            controllerAs: 'productCtrl'
        });

    }])

    .controller('productCtrl', function ($scope, $timeout, userService, productService, cartService, $mdToast, WorkerService, $mdDialog) {
        'use strict';

        let self = this;

        self.image_loading = true;

        self.qty_possible = [];

        self.adding_to_cart = false;

        self.contact_menu_open = false;

        self.referenceUrl = "";

        self.product = {};

        let queryString = window.location.href ? window.location.href.split('?')[1] : window.location.search.slice(1);
        if(queryString){
            self.referenceUrl = queryString.split('=')[1];
        }else{
            window.history.back();
        }

        productService.getDetailsProduct(self.referenceUrl).then(function (data) {
            self.product = data;

            productService.getProductsWorker(self.product.artisan.id).then(products => {
                let index = products.findIndex(p => p.reference === self.product.reference);

                if (index > -1) {
                    products.splice(index, 1);
                }

                products.forEach(p => {
                    productService.getImage(p.reference).then(img => {
                        p.image = img;
                        self.other_products.push(p);
                    });
                });

                for (let i = 1; i <= self.product.quantiteStock; i++) {
                    self.qty_possible.push(i);
                }

                productService.getImage(self.product.reference).then(response => {
                    self.product.image = response;
                    self.image_loading = false;
                });

                WorkerService.getImage(self.product.artisan.id).then(function (response) {
                    self.product.artisan.image = response.data;
                });
            });

        }).catch(function () {
            window.history.back();
        });

        self.other_products = [];

        self.disableAddProduct = function (qty) {
            return !qty || qty === 'none';
        };

        self.openPhonePopup = function(p_number) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('body')))
                    .clickOutsideToClose(true)
                    .title('Contacter cet artisan')
                    .textContent(`Vous pouvez contacter cet artisan au ${p_number}. N'hésitez pas à dire que vous venez du site Yateli !`)
                    .ok('Fermer')
            );
        };

        self.addToCart = function (ref, qty) {

            if (userService.getCart() === null && userService.getUserObject()) {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Vous n\'avez pas de panier, merci de contacter un admin')
                        .position('bottom right')
                        .hideDelay(3000));
                return;
            }

            self.adding_to_cart = true;
            cartService.addToCart({quantite: qty, reference: ref}).then(response => {
                userService.setCart(response.data);
                self.adding_to_cart = false;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Produit ajouté au panier !')
                        .position('bottom right')
                        .hideDelay(3000));
            }, error => {
                console.error('error', error);
                self.adding_to_cart = false;
                if (error.status === 401) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Merci de vous connecter')
                            .position('bottom right')
                            .hideDelay(3000));
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(error.message)
                            .position('bottom right')
                            .hideDelay(3000));
                }
            });
        };

    });