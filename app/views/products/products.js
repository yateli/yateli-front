angular.module('yateliApp.products', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/products', {
            templateUrl: 'views/products/products.html',
            controller: 'productsCtrl',
            controllerAs: 'productsCtrl',
            css: 'views/products/products.css'
        });

    }])

    .controller('productsCtrl', function ($scope, userService, $mdToast, cartService, productService) {
        'use strict';
        let self = this;

        self.productList = [];

        self.invalidQty = function (qty) {
            return qty < 1;
        };

        const worker_id = getAllUrlParams(window.location.href).id;

        if (worker_id) {
            productService.getProductsWorker(worker_id).then(products => {
                console.log('new method products', products);
                products.forEach(prod => {
                    if (prod.artisan.id === parseInt(worker_id)) {
                        prod.selectQty = false;
                        productService.getImage(prod.reference).then(function (image) {
                            prod.imageProduct = image;
                        }).catch(function (error) {
                            console.error(error.data.message);
                        });
                        self.productList.push(prod);
                    }
                });
            });
        } else {
            productService.getAllProducts().then(function (data) {
                data.data.forEach(prod => {
                    prod.selectQty = false;
                    productService.getImage(prod.reference).then(function (image) {
                        prod.imageProduct = image;
                        self.productList.push(prod);
                    }).catch(function (error) {
                        console.error(error.data.message);
                    });
                });
            });
        }




        self.addToCart = function(item, qty) {
            let body = {
                reference: item.reference,
                quantite: qty
            };
            cartService.addToCart(body).then(function (response) {
                if (response.status !== 401) {
                    userService.setCart(response.data);
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('C\'est ajouté !')
                            .position("bottom right")
                            .hideDelay(3000)
                    );
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Merci de vous connecter')
                            .position("bottom right")
                            .hideDelay(3000)
                    );
                }
            }, function (error) {
                console.error("error is", error);
                return error;
            });
        };

        self.redirectProduit = function (reference) {
            window.location.href = window.location.origin + "/#!/product?reference=" + reference;
        };

        function getAllUrlParams(url) {

            var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

            var obj = {};

            if (queryString) {

                queryString = queryString.split('#')[0];

                var arr = queryString.split('&');

                for (var i = 0; i < arr.length; i++) {
                    var a = arr[i].split('=');

                    var paramName = a[0];
                    var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

                    paramName = paramName.toLowerCase();
                    if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

                    if (paramName.match(/\[(\d+)?\]$/)) {

                        var key = paramName.replace(/\[(\d+)?\]/, '');
                        if (!obj[key]) obj[key] = [];

                        if (paramName.match(/\[\d+\]$/)) {
                            var index = /\[(\d+)\]/.exec(paramName)[1];
                            obj[key][index] = paramValue;
                        } else {
                            obj[key].push(paramValue);
                        }
                    } else {
                        if (!obj[paramName]) {
                            obj[paramName] = paramValue;
                        } else if (obj[paramName] && typeof obj[paramName] === 'string'){
                            obj[paramName] = [obj[paramName]];
                            obj[paramName].push(paramValue);
                        } else {
                            obj[paramName].push(paramValue);
                        }
                    }
                }
            }

            return obj;
        }


    });