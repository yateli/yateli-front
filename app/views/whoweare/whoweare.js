angular.module('yateliApp.whoweare', ['ngRoute', 'ngCookies'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/whoweare', {
            templateUrl: 'views/whoweare/whoweare.html',
            css: 'views/whoweare/whoweare.css',
            controller: 'whoweareCtrl'
        });

    }])

    .controller('whoweareCtrl', function ($scope, $timeout, userService, $cookies) {
        'use strict';



    });