angular.module('yateliApp.activateAccount', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/activerCompte', {
            templateUrl: 'views/activate-account/activate-account.html',
            css: 'views/activate-account/activate-account.css',
            controller: 'activateAccountCtrl',
            controllerAs: 'activateAccountCtrl'
        });

    }])

    .controller('activateAccountCtrl', function ($scope, $mdDialog, userService) {
        'use strict';

        let self = this;

        self.activating = false;
        self.erreur = null;
        self.login = null;

        self.parseUrlMail = parse_query_string(window.location.hash.split("?")[1]);

        if(self.parseUrlMail !== null){
            self.login = self.parseUrlMail["login"];
        }

        if(self.login == null || self.login === ""){
            window.location.href = "/#!/map";
        }

        self.activate = function () {
            if($scope.password === null || $scope.password === ""){
                self.erreur = "Veuillez renseigner un mot de passe !";
            }

            self.activating = true;
            userService.activateAccount(self.login,$scope.password).then(function (response) {
                self.activating = false;
                if(response.status === 200){
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('body')))
                            .clickOutsideToClose(true)
                            .title('Compte activé !')
                            .textContent('Merci d\'avoir activé votre compte. Rendez vous sur la page de connexion.')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('SE CONNECTER')
                    ).then(function () {
                        window.location.href = "/#!/connection";
                    });
                } else {
                    if(response.data.exception === "UserDejaActiveException"){
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#activate-account-container')))
                                .clickOutsideToClose(true)
                                .title('Compte déjà activé !')
                                .textContent('Votre compte a déjà été activé. Rendez vous sur la page de connexion.')
                                .ariaLabel('Alert Dialog Demo')
                                .ok('SE CONNECTER')
                        ).then(function () {
                            window.location.href = "/#!/connection";
                        });
                    }else{
                        self.erreur = response.data.message;
                    }
                }
            });
        };

        $(window).keydown(function(event){
            if(event.keyCode === 13) {
                self.activate();
            }
        });

        function parse_query_string(query) {
            if(query != null && query !== ""){
                var vars = query.split("&");
                var query_string = {};
                for (var i = 0; i < vars.length; i++) {
                    var pair = vars[i].split("=");
                    var key = decodeURIComponent(pair[0]);
                    var value = decodeURIComponent(pair[1]);
                    // If first entry with this name
                    if (typeof query_string[key] === "undefined") {
                        query_string[key] = decodeURIComponent(value);
                        // If second entry with this name
                    } else if (typeof query_string[key] === "string") {
                        query_string[key] = [query_string[key], decodeURIComponent(value)];
                        // If third or later entry with this name
                    } else {
                        query_string[key].push(decodeURIComponent(value));
                    }
                }
                return query_string;
            }else{
                return null;
            }
        }

    });