'use strict';

angular.module('yateliApp.version', [
  'yateliApp.version.interpolate-filter',
  'yateliApp.version.version-directive'
])

.value('version', '0.1');
