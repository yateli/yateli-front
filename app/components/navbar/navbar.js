

angular.module('yateliApp').component('yateliNavbar', {
    templateUrl: 'components/navbar/navbar.html',
    controller: navbarController,
    controllerAs: "navbarCtrl",
    bindings: {
        user: '=',
        sentence: '='
    }
});

navbarController.$inject = ["$scope", "$timeout", "userService", "webSocketService", "NotificationService", "$mdSidenav", "$cookies"];

function navbarController($scope, $timeout, userService, webSocketService, NotificationService, $mdSidenav, $cookies) {

    let self = this;

    self.notifications = [];

    self.showCart = true;

    self.notifications_unseen = 0;

    self.correctProfile = 'profile';

    function getNbItem(user) {
        if (user && user.hasOwnProperty("panier")) {
            self.qte = 0;
            if (!user.panier) {
                /*userService.setCart({'produits': []});
                user.panier = {'produits': []};*/
                self.showCart = false;
            } else {
                user.panier.produits.forEach(item => {
                    self.qte += item.quantite;
                });
            }
        }
    }

    self.disconnectUser = function() {
        webSocketService.disconnect();
        userService.disconnectUser(true);
    };

    $scope.$watch(function () {
        return NotificationService.getNotifications();
    }, function (newVal, oldVal, scope) {
        if(newVal){
            console.log('new notif', newVal);
            if (newVal) {
                self.notifications_unseen = 0;
                self.notifications = newVal;
                newVal.forEach(notif => {
                    if (!notif.vu) {
                        self.notifications_unseen++;
                    }
                });
            }
        }
    }, true);

    self.viewNotif = function(id) {
        NotificationService.seeNotification(id).then(response => {
            console.log('response see notif', response);
        });
    };

    self.deleteNotif = function(id) {
        NotificationService.removeNotification(id).then(response => {
            console.log('response delete notif', response);
        });
    };

    $scope.$watch(function () {
        return userService.getUserObject();
    }, function (newVal, oldVal, scope) {
        if (newVal) {
            self.user = newVal;
            console.log('user', newVal);
            switch (newVal.role) {
                case 'ADMIN':
                    self.correctProfile = 'profileAdmin';
                    break;
                case 'ARTISAN':
                    self.correctProfile = 'profileWorker';
                    break;
                default:
                    self.correctProfile = 'profile';
                    break;
            }
            getNbItem(newVal);
        }
    }, true);

    self.getDate = function(notification) {
        let str = '';
        let today = new Date();
        let date = notification.vu ? (str = 'Vu: ', new Date(notification.timestamp_vu)) : (str = '', new Date(notification.timestamp_creation));
        return str + (date.toLocaleDateString() === today.toLocaleDateString() ? `${date.getHours()}:${date.getMinutes()}` : `${date.toLocaleDateString()}`);

    };

    $scope.toggleSidenav = buildToggler('left');

    function buildToggler(componentId) {
        return function() {
            $mdSidenav(componentId).toggle();
        };
    }

    $scope.onSwipeLeft = function(ev, target) {
        $scope.toggleSidenav();
    };

}