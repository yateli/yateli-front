# Welcome to Yateli's Git repo !
Yateli is a **responsible** commercial website that sells clothes and accessories from all around the world !


# Technos

## AngularJS
AngularJS is a Javascript framework developed by Google that makes front end development more easy.

https://angularjs.org/

## AngularJS Material
Angular Material is a Material Design CSS Framework that follows the Material Design rules.

https://material.angularjs.org/latest/

## Mapbox and AngularJS directive for Mapbox
Used to display a map on the homepage. The AngularJS directive makes it work with AngularJS.

https://www.mapbox.com/

https://naimikan.github.io/angular-mapboxgl-directive/#/


# Set up
`npm install` to install all dependencies

# Run
`npm start` run local server. Each modification will be taken in consideration when refreshing the page.
Node and npm are required.
